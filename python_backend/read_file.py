import pandas as pd
import ast


# import openpyxl
# from datetime import datetime


def read_xlsx(file, command_row):
    sheet_name = 0
    header = 0
    names = None
    index_col = None
    usecols = None
    squeeze = False
    dtype = None
    engine = None
    converters = None
    true_values = None
    false_values = None
    skiprows = None
    nrows = None
    na_values = None
    keep_default_na = True
    verbose = False
    parse_dates = False
    date_parser = None
    thousands = None
    comment = None
    skipfooter = 0
    convert_float = True
    mangle_dupe_cols = True

    # “xlrd” supports old-style Excel files (.xls).
    # “openpyxl” supports newer Excel file formats.
    # “odf” supports OpenDocument file formats (.odf, .ods, .odt).
    # “pyxlsb” supports Binary Excel files.
    file_extension = file.rsplit('.', 1)[1]
    if file_extension == 'xls':
        engine = "xlrd"
    elif file_extension in ['odf', 'ods', 'odt']:
        engine = "odf"
    elif file_extension == 'xlsb':
        engine = "pyxlsb"
    else:
        engine = 'openpyxl'

    for key in command_row.keys():
        # print(key)
        if key == 'header':
            header = ast.literal_eval(command_row['header'])
            if header == 0:
                header = None
            # convert list like string to list
            # header = header)
            # convert 1-index to 0-index
            if isinstance(header, (int, float)):
                header = int(header - 1)
            elif isinstance(header, list):
                header = [x - 1 for x in header]
        elif key == 'sheet name':
            sheet_name = ast.literal_eval(command_row['sheet name'])
            if isinstance(sheet_name, (int, float)):
                sheet_name = sheet_name - 1
            elif isinstance(sheet_name, str):
                if sheet_name.lower() == 'all':
                    sheet_name = None
            elif isinstance(sheet_name, list):
                sheet_name = [(x - 1) if isinstance(x, (int, float)) else x for x in sheet_name]
        # elif key == 'engine':
        #     if isinstance(command_row['engine'], str):
        #         engine = command_row['engine']
        #     else:
        #         engine = ast.literal_eval(command_row['engine'])
        elif key == 'names':
            names = ast.literal_eval(command_row['names'])
        elif key == 'index_col':
            ## column to use as row index in data frame
            ## not implemented as no business use yet
            pass
        elif key == 'use columns':
            # usecols = command_row['use cols'][1:-1] # ast.literal_eval(command_row['use cols']).lower()
            usecols = ast.literal_eval(command_row['use cols'])[0]
        elif key == 'skiprows':
            skiprows = ast.literal_eval(command_row['skiprows'])
            if isinstance(skiprows, (int, float)):
                # skiprows = int(skiprows - 1)
                pass
            elif isinstance(skiprows, list):
                skiprows = [(x - 1) for x in skiprows]
        elif key == 'null values':
            na_values = ast.literal_eval(command_row['null values'])
        elif key == 'keep default null':
            if ast.literal_eval(command_row['keep default null']).lower() == 'false':
                keep_default_na = False
        elif key == '':
            pass
        elif key == 'number of rows':
            nrows = ast.literal_eval(command_row['number of rows'])

    df = pd.read_excel(file, sheet_name=sheet_name, header=header, names=names, index_col=index_col, usecols=usecols,
                       squeeze=squeeze, dtype=dtype, engine=engine, converters=converters, true_values=true_values,
                       false_values=false_values, skiprows=skiprows, nrows=nrows, na_values=na_values,
                       keep_default_na=keep_default_na, verbose=verbose, parse_dates=parse_dates,
                       date_parser=date_parser, thousands=thousands, comment=comment, skipfooter=skipfooter,
                       convert_float=convert_float, mangle_dupe_cols=mangle_dupe_cols)

    if not isinstance(header, int) and header:  # is multi header
        df.columns = [str(x) + ' ' + str(y) for x, y in list(df.columns)]
        col_headers = df.columns.to_list()
        # col_header = [x[19:] if 'Unnamed' in x else x for x in col_header]
        # col_header = [x.replace('_level_1', '') for x in col_header]
        # col_header = [x.replace('0_level_0', '') for x in col_header]
        # col_header = [x.replace(':', '') for x in col_header]
        for col_headers_index in range(len(col_headers)):
            col_header = col_headers[col_headers_index]
            if 'Unnamed:' in col_header:
                start_index = col_header.index('Unnamed:')
                end_index = col_header.rindex('_level_') + 8
                # print(col_header[0:start_index] + x[end_index + 8:])
                col_header = col_header[0:start_index] + col_header[end_index:]
                col_headers[col_headers_index] = col_header.strip()
            else:
                pass
        df.columns = col_headers

    # convert dict of df to single df, case: reading multiple dfs
    if isinstance(sheet_name, list) or sheet_name is None:
        df = pd.concat(df, ignore_index=True)
    return df


# def read_xlsb(file_name, sheet_name, header=0, usecols=None, skiprows=None, skipfooter=0, nrows=None
#               , na_values=None, keep_default_na=True):
#     # str to list
#     if not isinstance(header, (int, float)):
#         header = ast.literal_eval(header)
#     # convert 1 index to 0 index
#     if isinstance(header, (int, float)):
#         header = int(header - 1)
#     else:
#         header = [x - 1 for x in header]
#     if sheet_name.lower().lower() == 'all':
#         df = pd.concat(pd.read_excel(file_name, sheet_name=None, header=header, usecols=usecols, skiprows=skiprows
#                        , skipfooter=skipfooter, nrows=nrows, na_values=na_values, keep_default_na=keep_default_na
#                        , engine='pyxlsb'), ignore_index=True)
#     else:
#         df = pd.read_excel(file_name, sheet_name=sheet_name, header=header, usecols=usecols, skiprows=skiprows
#                        , skipfooter=skipfooter, nrows=nrows, na_values=na_values, keep_default_na=keep_default_na
#                        , engine='pyxlsb')
#
#
#     return df


def read_csv(file, command_row):
    sep = ','
    header = 0
    names = None
    index_col = None
    usecols = None
    squeeze = False
    prefix = None
    mangle_dupe_cols = True
    dtype = None
    engine = None
    converters = None
    true_values = None
    false_values = None
    skipinitialspace = False
    skiprows = None
    skipfooter = 0
    nrows = None
    na_values = None
    keep_default_na = True
    na_filter = True
    verbose = False
    skip_blank_lines = True
    parse_dates = False
    infer_datetime_format = False
    keep_date_col = False
    date_parser = None
    dayfirst = False
    cache_dates = True
    iterator = False
    chunksize = None
    compression = 'infer'
    thousands = None
    decimal = '.'
    lineterminator = None
    quotechar = '"'
    quoting = 0
    doublequote = True
    escapechar = None
    comment = None
    encoding = None
    dialect = None
    error_bad_lines = True
    warn_bad_lines = True
    delim_whitespace = False
    low_memory = True
    memory_map = False
    float_precision = None

    for key in command_row.keys():
        # print(key)
        if key == 'separator':
            # print(command_row['sep'][1:-1])
            sep = command_row['separator'][1:-1]
        elif key == 'header':
            header = ast.literal_eval(command_row['header'])
            # convert list like string to list
            # header = header)
            # convert 1-index to 0-index
            if header is None:
                pass
            elif isinstance(header, (int, float)):
                header = int(header - 1)
            else:
                header = [x - 1 for x in header]
        elif key == 'skip rows':
            skiprows = ast.literal_eval(command_row['skip rows'])
            if isinstance(skiprows, (int, float)):
                skiprows = int(skiprows - 1)
            else:
                skiprows = [x - 1 for x in skiprows]
        elif key == 'decimal':
            decimal = ast.literal_eval(command_row['decimal'])
        elif key == 'encoding':
            encoding = ast.literal_eval(command_row['encoding'])
        elif key == 'select columns':
            usecols = ast.literal_eval(command_row['select columns'])
            if isinstance(usecols, (list)):
                usecols = [x - 1 for x in usecols]
        elif key == 'set column name':
            names = ast.literal_eval(command_row['set column name'])
        elif key == 'null values':
            na_values = ast.literal_eval(command_row['null values'])
        elif key == 'keep default null':
            if ast.literal_eval(command_row['keep default null']).lower() == 'false':
                keep_default_na = False
        elif key == 'thousands':
            thousands = ast.literal_eval(command_row['thousands'])
        elif key == 'number of rows':
            nrows = ast.literal_eval(command_row['number of rows'])
        elif key == 'compression':
            compression = ast.literal_eval(command_row['compression'])

    df = pd.read_csv(file, sep=sep, header=header, names=names, index_col=index_col, usecols=usecols, squeeze=squeeze,
                     prefix=prefix, mangle_dupe_cols=mangle_dupe_cols, dtype=dtype, engine=engine,
                     converters=converters,
                     true_values=true_values, false_values=false_values, skipinitialspace=skipinitialspace,
                     skiprows=skiprows, skipfooter=skipfooter, nrows=nrows, na_values=na_values,
                     keep_default_na=keep_default_na, na_filter=na_filter, verbose=verbose,
                     skip_blank_lines=skip_blank_lines, parse_dates=parse_dates,
                     infer_datetime_format=infer_datetime_format,
                     keep_date_col=keep_date_col, date_parser=date_parser, dayfirst=dayfirst, iterator=iterator,
                     chunksize=chunksize, compression=compression, thousands=thousands, decimal=decimal,
                     lineterminator=lineterminator, quotechar=quotechar, quoting=quoting, doublequote=doublequote,
                     escapechar=escapechar, comment=comment, encoding=encoding, dialect=dialect,
                     error_bad_lines=error_bad_lines, warn_bad_lines=warn_bad_lines, delim_whitespace=delim_whitespace,
                     low_memory=low_memory, memory_map=memory_map, float_precision=float_precision)

    if not isinstance(header, int) and header:  # is multi header
        df.columns = [str(x) + ' ' + str(y) for x, y in list(df.columns)]
        col_headers = df.columns.to_list()
        # col_header = [x[19:] if 'Unnamed' in x else x for x in col_header]
        # col_header = [x.replace('_level_1', '') for x in col_header]
        # col_header = [x.replace('0_level_0', '') for x in col_header]
        # col_header = [x.replace(':', '') for x in col_header]
        for col_headers_index in range(len(col_headers)):
            col_header = col_headers[col_headers_index]
            if 'Unnamed:' in col_header:
                start_index = col_header.index('Unnamed:')
                end_index = col_header.rindex('_level_') + 8
                # print(col_header[0:start_index] + x[end_index + 8:])
                col_header = col_header[0:start_index] + col_header[end_index:]
                col_headers[col_headers_index] = col_header.strip()
            else:
                pass
        df.columns = col_headers
    return df


def read_fwf(file, command_row):
    colspecs = 'infer'
    widths = None
    infer_nrows = 100

    sep = ','
    header = 0
    names = None
    index_col = None
    usecols = None
    squeeze = False
    prefix = None
    mangle_dupe_cols = True
    dtype = None
    engine = None
    converters = None
    true_values = None
    false_values = None
    skipinitialspace = False
    skiprows = None
    skipfooter = 0
    nrows = None
    na_values = None
    keep_default_na = True
    na_filter = True
    verbose = False
    skip_blank_lines = True
    parse_dates = False
    infer_datetime_format = False
    keep_date_col = False
    date_parser = None
    dayfirst = False
    cache_dates = True
    iterator = False
    chunksize = None
    compression = 'infer'
    thousands = None
    decimal = '.'
    lineterminator = None
    quotechar = '"'
    quoting = 0
    doublequote = True
    escapechar = None
    comment = None
    encoding = None
    dialect = None
    error_bad_lines = True
    warn_bad_lines = True
    delim_whitespace = False
    low_memory = True
    memory_map = False
    float_precision = None

    for key in command_row.keys():
        # print(key)
        if key == 'index':
            # print(command_row['sep'][1:-1])
            colspecs = ast.literal_eval(command_row['index'])
        elif key == 'width':
            widths = ast.literal_eval(command_row['width'])
        elif key == 'header':
            header = ast.literal_eval(command_row['header'])
            # convert list like string to list
            # header = header)
            # convert 1-index to 0-index
            if header is None:
                pass
            elif isinstance(header, (int, float)):
                header = int(header - 1)
            else:
                header = [x - 1 for x in header]
        elif key == 'skip rows':
            skiprows = ast.literal_eval(command_row['skip rows'])
            if isinstance(skiprows, (int, float)):
                skiprows = int(skiprows - 1)
            else:
                skiprows = [x - 1 for x in skiprows]
        elif key == 'decimal':
            decimal = ast.literal_eval(command_row['decimal'])
        elif key == 'encoding':
            encoding = ast.literal_eval(command_row['encoding'])
        elif key == 'select columns':
            usecols = ast.literal_eval(command_row['select columns'])
            if isinstance(usecols, (list)):
                usecols = [x - 1 for x in usecols]
        elif key == 'set column name':
            names = ast.literal_eval(command_row['set column name'])
        elif key == 'null values':
            na_values = ast.literal_eval(command_row['null values'])
        elif key == 'keep default null':
            if ast.literal_eval(command_row['keep default null']).lower() == 'false':
                keep_default_na = False
        elif key == 'thousands':
            thousands = ast.literal_eval(command_row['thousands'])
        elif key == 'number of rows':
            nrows = ast.literal_eval(command_row['number of rows'])
        elif key == 'compression':
            compression = ast.literal_eval(command_row['compression'])

    df = pd.read_fwf(file, colspecs=colspecs, widths=widths, infer_nrows=infer_nrows, header=header, names=names
                     , index_col=index_col, usecols=usecols, squeeze=squeeze,
                     prefix=prefix, mangle_dupe_cols=mangle_dupe_cols, dtype=dtype, engine=engine,
                     converters=converters,
                     true_values=true_values, false_values=false_values, skipinitialspace=skipinitialspace,
                     skiprows=skiprows, skipfooter=skipfooter, nrows=nrows, na_values=na_values,
                     keep_default_na=keep_default_na, na_filter=na_filter, verbose=verbose,
                     skip_blank_lines=skip_blank_lines, parse_dates=parse_dates,
                     infer_datetime_format=infer_datetime_format,
                     keep_date_col=keep_date_col, date_parser=date_parser, dayfirst=dayfirst, iterator=iterator,
                     chunksize=chunksize, compression=compression, thousands=thousands, decimal=decimal,
                     lineterminator=lineterminator, quotechar=quotechar, quoting=quoting, doublequote=doublequote,
                     escapechar=escapechar, comment=comment, encoding=encoding, dialect=dialect,
                     error_bad_lines=error_bad_lines, warn_bad_lines=warn_bad_lines, delim_whitespace=delim_whitespace,
                     low_memory=low_memory, memory_map=memory_map, float_precision=float_precision)

    if not isinstance(header, int) and header:  # is multi header
        df.columns = [str(x) + ' ' + str(y) for x, y in list(df.columns)]
        col_headers = df.columns.to_list()
        # col_header = [x[19:] if 'Unnamed' in x else x for x in col_header]
        # col_header = [x.replace('_level_1', '') for x in col_header]
        # col_header = [x.replace('0_level_0', '') for x in col_header]
        # col_header = [x.replace(':', '') for x in col_header]
        for col_headers_index in range(len(col_headers)):
            col_header = col_headers[col_headers_index]
            if 'Unnamed:' in col_header:
                start_index = col_header.index('Unnamed:')
                end_index = col_header.rindex('_level_') + 8
                # print(col_header[0:start_index] + x[end_index + 8:])
                col_header = col_header[0:start_index] + col_header[end_index:]
                col_headers[col_headers_index] = col_header.strip()
            else:
                pass
        df.columns = col_headers

    return df
