import ast
import io
import os
import re
import sys
import traceback
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
import xlrd

import read_file as module_read_file
import config as module_configuration


def read_file(command_row):
    # print(command_row)
    file_path = command_row['file path'].replace('\\', '/')
    file_path = ast.literal_eval(file_path)
    file_name = ast.literal_eval(command_row['file name'])
    if file_path[-1] == '/':
        file_path = file_path[:-1]

    df = pd.DataFrame()
    if isinstance(file_name, str):
        files = os.listdir(file_path)
        file_list = [file for file in files if re.search(file_name, file) and '~$' not in file]
        if len(file_list) == 0:
            print('File name: ' + str(command_row["file name"]))
            print('Skipping as no file available')
            print()
            return None
        if len(file_list) > 1:
            print('Multiple files found:  ', file_list)
            return None

        file_name = file_list[0]
        extension = file_name.rsplit('.', 1)[1].lower()

        if extension == 'xlsb':
            command_row['engine'] = 'pyxlsb'
        if extension == 'xlsx' or extension == 'xls' or extension == 'xlsm' or extension == 'xlsb':
            df = rf.read_xlsx(file_path + '/' + file_name, command_row)
        elif extension == 'csv' or extension == 'txt':
            df = rf.read_csv(file_path + '/' + file_name, command_row)
        else:
            raise Exception("Error: File format did not match! Please upload only xlsx, xls, xlsm, xlsb, csv, "
                            "or txt(delimeter separated) format")

    elif isinstance(file_name, list):
        # print(file_name)
        if 'add_date' in file_name[0]:
            file_name_with_date_template = file_name[0]
            number_of_file = file_name[1]
            file_name_with_date = []
            file_name_with_date_template = file_name_with_date_template.replace('+', '')
            if 'date' in command_row.keys():
                current_date = datetime.strptime(ast.literal_eval(command_row['date']), '%m/%d/%Y')
            else:
                current_date = datetime.now()
            start_index = file_name_with_date_template.find('add_date(') + 9
            end_index = file_name_with_date_template.find(')', start_index)
            date_format = file_name_with_date_template[start_index: end_index]
            for date_delta in range(number_of_file):
                # print(date_delta)
                # print(datetime.strftime(current_date - timedelta(days=date_delta), y))
                date_in_file_name = datetime.strftime(current_date - timedelta(days=date_delta), date_format)
                print(file_name_with_date_template.replace('add_date(%Y%m%d)', date_in_file_name))
                file_name_with_date.append(
                    file_name_with_date_template.replace('add_date(' + date_format + ')', date_in_file_name))
            file_name = file_name_with_date

        elif str(file_name[0]).lower().strip() == 'all':
            from os import listdir
            from os.path import isfile, join
            file_name = [f for f in listdir(file_path) if isfile(join(file_path + '/', f)) and '~$' not in f]

        for name in file_name:
            extension = name.rsplit('.', 1)[1].lower()
            try:
                if extension == 'xlsb':
                    command_row['engine'] = 'pyxlsb'
                if extension == 'xlsx' or extension == 'xls' or extension == 'xlsm' or extension == 'xlsb':
                    df_temp = rf.read_xlsx(file_path + '/' + name, command_row)
                elif extension == 'csv' or extension == 'txt':
                    df_temp = rf.read_csv(file_path + '/' + name, command_row)
                else:
                    raise Exception("Error: File format did not match! Please upload only xlsx, xls, xlsm, xlsb, csv, "
                                    "or txt(delimeter separated) format")
                if 'fill column value' in command_row.keys():
                    pass  # ['Wholesaler', 'cell=[6,5], 'File name']
                    fill_column_value_details = ast.literal_eval(command_row['fill column value'])
                    for i in range(1, len(fill_column_value_details)):
                        if 'cell' in fill_column_value_details[i].lower().strip():
                            val = ast.literal_eval(fill_column_value_details[i].split('=')[1])
                        elif fill_column_value_details[i].lower().strip() == 'file name':
                            df_temp[fill_column_value_details[0]] = name.rsplit('.', 1)[0]

                df = df.append(df_temp, ignore_index=True)
            except FileNotFoundError:
                print("The following file was attempted to be processed but could not be found: ", name)

    if not df.empty:
        col_names = list(df.columns)
        col_names = [str(col_name).strip() for col_name in col_names]
        df.columns = col_names
        if 'select columns as' in command_row.keys():
            # col_names = list(df.columns)
            # col_names = [col_name.lower() for col_name in col_names]
            # df.columns = col_names

            rename_value = command_row['select columns as']
            rename_value = '{' + str(rename_value)[1:-1] + '}'
            rename_value = ast.literal_eval(rename_value)
            if pd.notna(rename_value) and rename_value != '':
                df.rename(columns=rename_value, inplace=True)
            df = df[list(rename_value.values())]
    return df


def read_fwf(command_row):
    """

    @param command_row:
    @return:
    """
    file_path = command_row['file path'].replace('\\', '/')
    file_path = ast.literal_eval(file_path)
    file_name = ast.literal_eval(command_row['file name'])
    if file_path[-1] == '/':
        file_path = file_path[:-1]

    df = pd.DataFrame()
    if isinstance(file_name, str):
        files = os.listdir(file_path)
        file_list = [file for file in files if re.search(file_name, file) and '~$' not in file]
        if len(file_list) == 0:
            print('File name: ' + str(command_row["file name"]))
            print('Skipping as no file available')
            print()
            return None
        if len(file_list) > 1:
            print('Multiple files found:  ', file_list)
            return None

        file_name = file_list[0]

        df = rf.read_fwf(file_path + '/' + file_name, command_row)

    elif isinstance(file_name, list):
        # print(file_name)
        if 'add_date' in file_name[0]:
            file_name_with_date_template = file_name[0]
            number_of_file = file_name[1]
            file_name_with_date = []
            file_name_with_date_template = file_name_with_date_template.replace('+', '')
            if 'date' in command_row.keys():
                current_date = datetime.strptime(ast.literal_eval(command_row['date']), '%m/%d/%Y')
            else:
                current_date = datetime.now()
            start_index = file_name_with_date_template.find('add_date(') + 9
            end_index = file_name_with_date_template.find(')', start_index)
            date_format = file_name_with_date_template[start_index: end_index]
            for date_delta in range(number_of_file):
                # print(date_delta)
                # print(datetime.strftime(current_date - timedelta(days=date_delta), y))
                date_in_file_name = datetime.strftime(current_date - timedelta(days=date_delta), date_format)
                print(file_name_with_date_template.replace('add_date(%Y%m%d)', date_in_file_name))
                file_name_with_date.append(
                    file_name_with_date_template.replace('add_date(' + date_format + ')', date_in_file_name))
            file_name = file_name_with_date

        elif str(file_name[0]).lower().strip() == 'all':
            from os import listdir
            from os.path import isfile, join
            file_name = [f for f in listdir(file_path) if isfile(join(file_path + '/', f)) and '~$' not in f]

        for name in file_name:
            df_temp = rf.read_fwf(file_path + '/' + file_name, command_row)
            df = df.append(df_temp, ignore_index=True)

    if not df.empty:
        col_names = list(df.columns)
        col_names = [str(col_name).strip() for col_name in col_names]
        df.columns = col_names
        if 'select columns as' in command_row.keys():
            # col_names = list(df.columns)
            # col_names = [col_name.lower() for col_name in col_names]
            # df.columns = col_names

            rename_value = command_row['select columns as']
            rename_value = '{' + str(rename_value)[1:-1] + '}'
            rename_value = ast.literal_eval(rename_value)
            if pd.notna(rename_value) and rename_value != '':
                df.rename(columns=rename_value, inplace=True)
            df = df[list(rename_value.values())]
    return df


def read_ftp(command_row):
    import pysftp
    ftp_loc = ast.literal_eval(command_row['ftp location'])
    credentials = ast.literal_eval(command_row['credentials'])
    file_path = ast.literal_eval(command_row['file path'])
    file_name = ast.literal_eval(command_row['file name'])
    if 'port' in command_row.keys():
        port = int(ast.literal_eval(command_row['port']))
    else:
        port = 22
    # ftp = FTP(ftp_loc)
    # ftp.login(credentials[0], credentials[1])
    # ftp.set_pasv(False)
    # ftp.cwd(file_path)
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    sftp = pysftp.Connection(host=ftp_loc, username=credentials[0], password=credentials[1], port=port, cnopts=cnopts)
    sftp.cwd(file_path)
    df = pd.DataFrame()
    if isinstance(file_name, str):
        files = sftp.listdir()
        file_list = [file for file in files if re.search(file_name, file)]
        if len(file_list) == 0:
            print('File name: ' + str(command_row["file name"]))
            print('Skipping as no file available')
            # print()
            return None
        if len(file_list) > 1:
            print('Multiple files found:  ', file_list)
            return None

        file_name = file_list[0]
        extension = file_name.rsplit('.', 1)[1].lower()

        try:
            file_object = io.BytesIO()
            sftp.getfo(file_name, file_object)
            file_object.seek(0)

            if extension == 'xlsb':
                command_row['engine'] = 'pyxlsb'
            if extension == 'xlsx' or extension == 'xls' or extension == 'xlsm' or extension == 'xlsb':
                df = rf.read_xlsx(file_object, command_row)
            elif extension == 'csv' or extension == 'txt':
                df = rf.read_csv(file_object, command_row)
            elif extension == 'gz':
                df = rf.read_csv(file_object, command_row)
            else:
                raise Exception("Error: File format did not match! Please upload only xlsx, xls, xlsm, xlsb, csv, "
                                "or txt(delimiter separated) format")
        except FileNotFoundError:
            print("The following file was attempted to be processed but could not be found: ", file_name)

    elif isinstance(file_name, list):
        # print(file_name)
        if 'add_date' in file_name[0]:
            file_name_with_date_template = file_name[0]
            number_of_file = file_name[1]
            file_name_with_date = []
            file_name_with_date_template = file_name_with_date_template.replace('+', '')
            if 'date' in command_row.keys():
                current_date = datetime.strptime(ast.literal_eval(command_row['date']), '%m/%d/%Y')
            else:
                current_date = datetime.now()
            start_index = file_name_with_date_template.find('add_date(') + 9
            end_index = file_name_with_date_template.find(')', start_index)
            date_format = file_name_with_date_template[start_index: end_index]
            for date_delta in range(number_of_file):
                # print(date_delta)
                # print(datetime.strftime(current_date - timedelta(days=date_delta), y))
                date_in_file_name = datetime.strftime(current_date - timedelta(days=date_delta), date_format)
                # print(file_name_with_date_template.replace('add_date(%Y%m%d)', date_in_file_name))
                file_name_with_date.append(
                    file_name_with_date_template.replace('add_date(' + date_format + ')', date_in_file_name))
            file_name = file_name_with_date

        for name in file_name:
            extension = name.rsplit('.', 1)[1].lower()

            # files = ftp.nlst()
            # if name not in files:
            #     print("The following file was attempted to be processed but could not be found: ", name)
            #     continue

            try:
                file_object = io.BytesIO()
                sftp.getfo(name, file_object)
                file_object.seek(0)

                if extension == 'xlsb':
                    command_row['engine'] = 'pyxlsb'
                if extension == 'xlsx' or extension == 'xls' or extension == 'xlsm' or extension == 'xlsb':
                    df_temp = rf.read_xlsx(file_object, command_row)
                elif extension == 'csv' or extension == 'txt':
                    df_temp = rf.read_csv(file_object, command_row)
                else:
                    raise Exception("Error: File format did not match! Please upload only xlsx, xls, xlsm, xlsb, csv, "
                                    "or txt(delimeter separated) format")
                df = df.append(df_temp, ignore_index=True)
                print(" Read file: " + name)
            except FileNotFoundError:
                print(" Error in reading file: " + name + ". File may not be present.")

    sftp.close()

    if not df.empty:
        col_names = list(df.columns)
        col_names = [col_name.strip() for col_name in col_names]
        df.columns = col_names
        if 'select columns as' in command_row.keys():
            # print('present')
            rename_value = command_row['select columns as']
            rename_value = '{' + str(rename_value)[1:-1] + '}'
            rename_value = ast.literal_eval(rename_value)
            if pd.notna(rename_value) and rename_value != '':
                df.rename(columns=rename_value, inplace=True)
            df = df[list(rename_value.values())]

    return df


def download_ftp_folder(command_row):
    # import urllib
    # import ftplib
    # from ftplib import FTP
    import pysftp

    ftp_loc = ast.literal_eval(command_row['ftp location'].replace('\\', '/'))
    credentials = ast.literal_eval(command_row['credentials'])
    folder_path = ast.literal_eval(command_row['folder path'].replace('\\', '/'))
    folder_name = ast.literal_eval(command_row['folder name'].replace('\\', '/'))
    destination_path = ast.literal_eval(command_row['destination path'].replace('\\', '/'))

    if isinstance(folder_name, list):
        # print(file_name)
        if 'add_date' in folder_name[0]:
            file_name_with_date_template = folder_name[0]
            date_delta = folder_name[1]
            file_name_with_date = []
            file_name_with_date_template = file_name_with_date_template.replace('+', '')
            if 'date' in command_row.keys():
                current_date = datetime.strptime(ast.literal_eval(command_row['date']), '%m/%d/%Y')
            else:
                current_date = datetime.now()
            start_index = file_name_with_date_template.find('add_date(') + 9
            end_index = file_name_with_date_template.find(')', start_index)
            date_format = file_name_with_date_template[start_index: end_index]

            date_in_file_name = datetime.strftime(current_date - timedelta(days=date_delta), date_format)
            folder_name = file_name_with_date_template.replace('add_date(' + date_format + ')', date_in_file_name)

    if 'destination file name' in command_row.keys():
        dest_file_name = ast.literal_eval(command_row['destination file name'].replace('\\', '/'))
        destination_path = destination_path + dest_file_name
    else:
        destination_path = destination_path + folder_name

    if 'port' in command_row.keys():
        port = int(ast.literal_eval(command_row['port']))
    else:
        port = 22

    #--------- code for sftp
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None

    # hostname = 'https://protect-de.mimecast.com/s/RCkSC83GLKH66w8BysnRsdV?domain=ftp.gers-sas.fr'
    # sftp_username = 'SANOFIAVENTIF'
    # sftp_pw = '341xq7xn'
    # Make connection to sFTP
    with pysftp.Connection(ftp_loc,
                           username=credentials[0],
                           password=credentials[1],
                           cnopts=cnopts
                           ) as sftp:
        # sftp.isfile('directory/file.csv')) ## TRUE
        ftp_file_path = folder_path+folder_name
        sftp.get(ftp_file_path, destination_path )
        # print(file) ## None
    #----------------------

    #---------arcive code--------
    # ftp = FTP(user=credentials[0], passwd=credentials[1], source_address=(ftp_loc))
    # ftp = FTP(source_address=(ftp_loc))
    # ftp.login(credentials[0], credentials[1])
    # ftp.set_pasv(False)
    # ftp.cwd(folder_path)

    # ftp.retrbinary("RETR " + folder_name, open(destination_path, 'wb').write)

    # ftp.close()
    return 'Y'


def df_operation(df, command_row):
    command_row = command_row.to_frame(name="value")
    command_row.index.name = 'key'
    command_row.reset_index(inplace=True)
    # command_row.rename(columns={'index': 'key'}, inplace=True)
    # if command_row.loc[0, 'key'] == 'command':
    #     if command_row.loc[1, 'key'] == 'table':
    index = 2
    while index < len(command_row):
        if command_row.loc[index, 'key'] == 'rename':
            rename_value = command_row.loc[index, 'value']
            rename_value = '{' + rename_value[1:-1] + '}'
            rename_value = ast.literal_eval(rename_value)
            if pd.notna(rename_value) and rename_value != '':
                df.rename(columns=rename_value, inplace=True)
        elif command_row.loc[index, 'key'] == 'filter':
            filters = ast.literal_eval(command_row.loc[index, 'value'])
            for fltr in filters:
                # try:
                #     df[fltr[0]].ffill(inplace=True)
                # except:
                #     pass
                col_value = df[fltr[0]]
                try:
                    col_value = col_value.str.lower()
                    col_value = col_value.str.strip()
                except Exception as e:
                    pass

                # if isinstance(fltr[2], str):
                #     filter_value = fltr[2]
                #     filter_value = filter_value.lower()
                #     filter_value = filter_value.strip()
                # else:
                #     filter_value = fltr[2]
                filter_value = fltr[2]
                try:
                    filter_value = filter_value.lower()
                    filter_value = filter_value.strip()
                except Exception as e:
                    pass

                if fltr[1] == '=':
                    df = df[col_value == filter_value].copy()
                elif fltr[1] == '!=':
                    df = df[col_value != filter_value].copy()
                elif fltr[1] == '>':
                    df = df[col_value > filter_value].copy()
                elif fltr[1] == '<':
                    df = df[col_value < filter_value].copy()
                elif fltr[1] == 'contains':
                    df = df[col_value.str.contains(filter_value, case=False)].copy()
                elif fltr[1] == 'not contains':
                    df = df[~col_value.str.contains(filter_value, case=False)].copy()
                elif fltr[1] == 'multiple =':
                    # filters = filter_value
                    # filters_list = filters.strip('][').split('&*&')
                    filters_list = filter_value  # ast.literal_eval(filter_value)
                    filters_list = [ele.strip().lower() for ele in filters_list]
                    df = df[col_value.isin(filters_list)].copy()
                elif fltr[1] == 'multiple !=':
                    # filters = filter_value
                    # filters_list = filters.strip('][').split('&*&')
                    filters_list = filter_value  # ast.literal_eval(filter_value)
                    filters_list = [ele.strip().lower() for ele in filters_list]
                    df = df[~col_value.isin(filters_list)].copy()
                else:
                    print('No filter applied')
        elif command_row.loc[index, 'key'] == 'drop columns':
            drop_cols = ast.literal_eval(command_row.loc[index, 'value'])
            df.drop(columns=drop_cols, inplace=True)
        elif command_row.loc[index, 'key'] == 'select columns':
            use_cols = ast.literal_eval(command_row.loc[index, 'value'])
            df = df[use_cols].copy()
        elif command_row.loc[index, 'key'] == 'arthematic':
            operations = ast.literal_eval(command_row.loc[index, 'value'])
            for operation in operations:
                if isinstance(operation[2], int) or isinstance(operation[2], float):
                    if operation[1] == '+':
                        df[operation[0]] = df[operation[0]].astype(float) + float(operation[2])
                    elif operation[1] == '-':
                        df[operation[0]] = df[operation[0]].astype(float) - float(operation[2])
                    elif operation[1] == '*':
                        df[operation[0]] = df[operation[0]].astype(float) * float(operation[2])
                    elif operation[1] == '/':
                        df[operation[0]] = df[operation[0]].astype(float) / float(operation[2])
                else:  # second argument is column name
                    if operation[1] == '+':
                        df[operation[0]] = df[operation[0]].astype(float) + df[operation[2]].astype(float)
                    elif operation[1] == '-':
                        df[operation[0]] = df[operation[0]].astype(float) - df[operation[2]].astype(float)
                    elif operation[1] == '*':
                        df[operation[0]] = df[operation[0]].astype(float) * df[operation[2]].astype(float)
                    elif operation[1] == '/':
                        df[operation[0]] = df[operation[0]].astype(float) / df[operation[2]].astype(float)
        elif command_row.loc[index, 'key'] == 'combine':
            combine_list = ast.literal_eval(command_row.loc[index, 'value'])
            for combine in combine_list:
                # if df[combine[0]].dtype == object or df[combine[1]].dtype == object:
                df[combine[0]] = df[combine[0]].apply(str)
                df[combine[1]] = df[combine[1]].apply(str)
                if len(combine) == 4:
                    sep = combine[3]
                else:
                    sep = ''
                df[combine[2]] = df[combine[0]] + str(sep) + df[combine[1]]
                # else:
                #     df[combine[2]] = df[combine[0]] + df[combine[1]]
        elif command_row.loc[index, 'key'] == 'group by':
            group_by = ast.literal_eval(command_row.loc[index, 'value'])
            by_columns = group_by[0]
            sum_columns = group_by[1]
            for column in by_columns:
                df[column].fillna('null_filled__for_grp_by__temp', inplace=True)
            # df.fillna('null_filled__for_grp_by__temp', inplace=True)
            for column in sum_columns:
                df[column] = df[column].astype(float)
            df = df.groupby(by_columns)[sum_columns].sum().reset_index()
            for column in by_columns:
                df[column].replace('null_filled__for_grp_by__temp', np.NAN, inplace=True)
        elif command_row.loc[index, 'key'] == 'make positive':
            make_positive = ast.literal_eval(command_row.loc[index, 'value'])
            if isinstance(make_positive, str):
                column_name = make_positive
                multiplier = 1
            else:
                column_name = make_positive[0]
                multiplier = make_positive[1]
            df = df.astype({column_name: 'float'})
            df[column_name] = df[column_name].abs()
            df[column_name] = df[column_name] * multiplier
        elif command_row.loc[index, 'key'] == 'extract digit':
            extract_digit_columns = ast.literal_eval(command_row.loc[index, 'value'])
            for extract_digit_column in extract_digit_columns:
                df[extract_digit_column] = df[extract_digit_column].astype(str)
                df[extract_digit_column] = df[extract_digit_column].str.extract('(\d+)')
                df[extract_digit_column] = df[extract_digit_column].astype(float)
        index += 1
    return df


def date_slice(df, command_row):
    command_row = command_row.to_frame(name="value")
    command_row.reset_index(inplace=True)
    command_row.rename(columns={0: 'key'}, inplace=True)
    if command_row.loc[0, 'key'] == 'command':
        if command_row.loc[1, 'key'] == 'table':
            index = 2
            # temp = command_row.loc[command_row['key'] == 'axis'].reset_index(drop=True).copy()
            # if temp.loc[0, 'value'] == 'column':
            temp = command_row.loc[command_row['key'] == 'column'].reset_index(drop=True).copy()
            temp.loc[0, 'value'] = ast.literal_eval(temp.loc[0, 'value'])
            df = df[pd.notnull(df[temp.loc[0, 'value']])].copy()  # remove null from date column
            df['5Y37_date'] = df[temp.loc[0, 'value']]
            temp = command_row.loc[command_row['key'] == 'date format'].reset_index(drop=True).copy()
            temp.loc[0, 'value'] = ast.literal_eval(temp.loc[0, 'value'])
            if temp.loc[0, 'value'] == 'excel dates':
                df['5Y37_date'] = [datetime(*xlrd.xldate_as_tuple(int(dates), 0)) for dates in df['5Y37_date']]
            elif temp.loc[0, 'value'].lower().strip() == 'date' or temp.loc[0, 'value'].lower().strip() == 'dates':
                if not isinstance(df['5Y37_date'][0], datetime):
                    df['5Y37_date'] = pd.to_datetime(df['5Y37_date'])
                # df['5Y37_date'] = [date_5Y37.split(' ')[0] for date_5Y37 in df['5Y37_date']]
                # df['5Y37_date'] = pd.to_datetime(df['5Y37_date'], format='%d-%m-%Y')   #remove this line
                # pass
            else:
                df['5Y37_date'] = pd.to_datetime(df['5Y37_date'], format=temp.loc[0, 'value'])
            temp = command_row.loc[command_row['key'] == 'date count'].reset_index(drop=True)
            date_count_params = temp.loc[0, 'value'].split(',')
            if len(date_count_params) == 1:
                date_length = int(date_count_params[0])
                is_order_descending = True
            else:
                date_length = int(date_count_params[0])
                is_order_descending = True if date_count_params[1].lower().strip() == 'descending' else False
            # date_length, order =
            # date_length = int(date_length)
            # order = True if order.lower().strip() == 'descending' else False
            dates_list = list(set(df['5Y37_date']))
            if date_length < len(dates_list):
                dates_list.sort(reverse=is_order_descending)
                dates_list = dates_list[0:date_length]
                df = df[df['5Y37_date'].isin(dates_list)]
            df.drop(columns=['5Y37_date'], inplace=True)
            # df.sort_values(by=['5Y37_date'], inplace=True, ascending=order)
    return df


def change_date_format(df, command_row):
    command_row['column'] = ast.literal_eval(command_row['column'])
    command_row['date format'] = ast.literal_eval(command_row['date format'])
    command_row['output format'] = ast.literal_eval(command_row['output format'])

    if len(df[pd.isna(df[command_row['column']])]) != 0:
        print('Removed ', len(df[pd.isna(df[command_row['column']])]), ' rows as date column was blank!')
    df_blank_date = df[pd.isnull(df[command_row['column']])].copy()
    df = df[pd.notnull(df[command_row['column']])].copy()

    # if command_row['axis'] == 'column':
    if command_row['date format'].lower().strip() == 'excel dates':
        df[command_row['column']] = [datetime(*xlrd.xldate_as_tuple(int(dates), 0)) for dates in
                                     df[command_row['column']]]
    elif command_row['date format'].lower().strip() == 'date' or command_row['date format'].lower().strip() == 'dates':
        # df[command_row['column_name']] = [date_5Y37.split(' ')[0] for date_5Y37 in df[command_row['column_name']]]
        # df[command_row['column_name']] = pd.to_datetime(df[command_row['column_name']], format='%d-%m-%Y')  # remove this line
        if not isinstance(df[command_row['column']][0], datetime):
            df[command_row['column']] = pd.to_datetime(df[command_row['column']])
    else:
        df[command_row['column']] = pd.to_datetime(df[command_row['column']], format=command_row['date format'])

    df[command_row['column']] = df[command_row['column']].dt.strftime(command_row['output format'])
    df = df.append(df_blank_date)
    return df


def merge_df(df_1, df_2, command_row):
    # print(command_row)
    command_row['join type'] = ast.literal_eval(command_row['join type'])
    command_row['left on'] = ast.literal_eval(command_row['left on'])
    command_row['right on'] = ast.literal_eval(command_row['right on'])

    # change data frame to string before merge
    left_on = []
    right_on = []
    for col_names in command_row['left on']:
        df_1[col_names] = [str(col_value).upper().strip() for col_value in df_1[col_names]]
        left_on.append(col_names)
    for col_names in command_row['right on']:
        df_2[col_names] = [str(col_value).upper().strip() for col_value in df_2[col_names]]
        right_on.append(col_names)

    if 'drop duplicates' in command_row.keys():
        drop_duplicates_left, drop_duplicates_right = command_row['drop duplicates'].split(',')
        drop_duplicates_left = drop_duplicates_left.strip().lower()[1:-1]
        drop_duplicates_right = drop_duplicates_right.strip().lower()[1:-1]
        if drop_duplicates_left == 'true':
            df_1.drop_duplicates(subset=left_on, inplace=True)
        if drop_duplicates_right == 'true':
            df_2.drop_duplicates(subset=right_on, inplace=True)

    df_merged = df_1.merge(df_2, how=command_row['join type'], left_on=left_on, right_on=right_on)
    if 'select columns' in command_row.keys():
        df_merged = df_merged[ast.literal_eval(command_row['select columns'])]
    # df_2.to_csv('C:/Users/Syed/Desktop/Work/bi_road_map/new_2_yrs/output/2018_07/df_2.csv', index=False)
    # df_merged.drop(left_on, inplace=True)
    # df_merged.drop(right_on, inplace=True)

    return df_merged


def add_column(df, command_row):
    value = ast.literal_eval(command_row['value'])
    column = ast.literal_eval(command_row['column'])
    # if value[0] != "'":
    #     value = int(value)
    # else:
    #     value = value[1:-1]
    if isinstance(column, list):
        for i in range(len(column)):
            df[column[i]] = value[i]
    else:
        df[column] = value
    return df


def add_column_copy(df, command_row):
    value = ast.literal_eval(command_row['value'])
    df[ast.literal_eval(command_row['column'])] = df[value]
    return df


def fill_na(df, command_row):
    column_names = ast.literal_eval(command_row['column'])
    values = ast.literal_eval(command_row['value'])
    for index in range(0, len(column_names)):
        value = values[index]
        if isinstance(value, str) and value[0:4] == 'col=':
            value = value[4:]
            df[column_names[index]].fillna(df[value], inplace=True)
        else:
            df[column_names[index]].fillna(value, inplace=True)
    return df


def pivot(df, command_row):
    vars = ast.literal_eval(command_row['rows'])
    var_name = ast.literal_eval(command_row['column'])
    value_name = ast.literal_eval(command_row['value'])

    df = df.melt(id_vars=vars, var_name=var_name, value_name=value_name).copy()
    return df


def split_column(df, command_row):
    # print(command_row)
    column = ast.literal_eval(command_row['column'])
    new_columns = ast.literal_eval(command_row['new columns'])
    split_val = ast.literal_eval(command_row['on'])

    df[column] = df[column].astype('str')
    df[new_columns] = df[column].str.split(split_val[0], split_val[1], expand=True)

    for col_name in new_columns:
        df[col_name] = df[col_name].str.strip()

    return df


def unpivot(df, command_row):
    index = ast.literal_eval(command_row['rows'])
    column = ast.literal_eval(command_row['column'])
    value = ast.literal_eval(command_row['value'])
    # df[value] = df[value].fillna(0)
    df[index] = df[index].fillna('pandas_null_value')
    df = df.pivot_table(index=index, columns=column)[value].reset_index()
    df[index] = df[index].replace('pandas_null_value', np.nan)
    # original_df = df.pivot(index='Day', columns='Company')['Closing Price'].reset_index()
    column_headers = df.columns.to_list()
    for column_header_index in range(len(column_headers)):
        column_header = column_headers[column_header_index]
        column_header = list(map(lambda x: str(x), column_header))
        # column_header = column_header.lambda(x:str(x))
        column_header = ''.join(column_header)
        column_header = column_header.strip()
        column_headers[column_header_index] = column_header
    df.columns = column_headers
    return df


def append_table(df_1, df_2, command_row):
    return df_1.append(df_2, ignore_index=True)


def replace(df, command_row):
    column_name = ast.literal_eval(command_row['column'])
    replace_list = ast.literal_eval(command_row['value'])
    for replace_element in replace_list:
        df[column_name].replace(replace_element[0], replace_element[1],
                                inplace=True)  # replace replace_element[0] with replace_element[1]
    return df


def replace_character(df, command_row):
    column_name = ast.literal_eval(command_row['column'])
    replace_list = ast.literal_eval(command_row['value'])
    for replace_element in replace_list:
        df[column_name] = df[column_name].str.replace(replace_element[0], replace_element[
            1])  # replace replace_element[0] with replace_element[1]
    return df


def drop_null(df, command_row):
    column_name = ast.literal_eval(command_row['column'])
    df.dropna(subset=column_name, how='all', inplace=True)
    return df


def combine_columns(df, command_row):
    # Columns = ['PRODUCTGROUP1', 'CATEGORY1'] Delimiter = ' ' Result column = 'CURRENTTREATMENT'
    columns = ast.literal_eval(command_row['columns'])
    delimiter = ast.literal_eval(command_row['delimiter'])  # [1:-1]
    result_column = ast.literal_eval(command_row['result column'])
    df[result_column] = df[columns[0]].astype(str) + delimiter + df[columns[1]].astype(str)
    return df


def get_max_value(df, command_row):
    column_name = ast.literal_eval(command_row['column'])
    result_column = ast.literal_eval(command_row['result column'])
    input_format = ast.literal_eval(command_row['input type'])
    if input_format.lower().strip() == 'int':
        dt = df[column_name].astype(int)
        max_val = max(dt)
    elif input_format == 'date':
        date_format = ast.literal_eval(command_row['date format'])
        if len(df[pd.isna(df[column_name])]) != 0:
            print('Removed ', len(df[pd.isna(df[column_name])]), ' rows as date column was blank!')
        df = df[pd.notnull(df[column_name])].copy()

        # if command_row['axis'] == 'column':
        if date_format.lower().strip() == 'excel dates':
            dt = [datetime(*xlrd.xldate_as_tuple(int(dates), 0)) for dates in df[column_name]]
        elif date_format.lower().strip() == 'date' or date_format.lower().strip() == 'dates':
            # df[command_row['column_name']] = [date_5Y37.split(' ')[0] for date_5Y37 in df[command_row['column_name']]]
            # df[command_row['column_name']] = pd.to_datetime(df[command_row['column_name']], format='%d-%m-%Y')  # remove this line
            if not isinstance(df[column_name][0], datetime):
                dt = pd.to_datetime(df[column_name])
        else:
            dt = df[column_name].apply(lambda x: datetime.strptime(x + ' 1', date_format + ' %w'))
            # dt = pd.to_datetime(df[column_name] + ' 1', format=date_format + ' %w')
        max_val = datetime.strftime(max(dt), date_format)
    else:
        dt = df[column_name].astype(str)
        max_val = max(dt)
    df[result_column] = max_val
    return df


def change_data_type(df, command_row):
    column_name = ast.literal_eval(command_row['column'])
    data_type = ast.literal_eval(command_row['change to'])
    df[column_name] = df[column_name].astype(data_type)
    return df


def drop_duplicate(df_drop_duplicate, command_row):
    column_subset = ast.literal_eval(command_row['columns'])[0]
    keep = ast.literal_eval(command_row['keep row'])[0]
    df_drop_duplicate.drop_duplicates(subset=column_subset, keep=keep, inplace=True, ignore_index=True)

    return df_drop_duplicate


def generate_row_id(df, command_row):
    df.reset_index(drop=True, inplace=True)
    df.reset_index(drop=False, inplace=True)
    column = command_row['column'][1:-1]
    df.rename(columns={'index': column}, inplace=True)
    return df


def generate_date_number(df, command_row):
    date_column = ast.literal_eval(command_row['date column'])
    number_column = ast.literal_eval(command_row['number column'])
    date_format = ast.literal_eval(command_row['date format'])
    order = ast.literal_eval(command_row['order'])
    date_list = list(set(df[date_column]))
    # date_list = list(set(df[date_column]))

    if str(date_format).lower().strip() == 'excel dates':
        date_list = [datetime(*xlrd.xldate_as_tuple(int(dates), 0)) for dates in date_list]
    elif str(date_format).lower().strip() == 'date' or str(date_format).lower().strip() == 'dates':
        if not isinstance(date_list[0], datetime):
            date_list = pd.to_datetime(date_list)
    else:
        date_list = pd.to_datetime(date_list, format=date_format)

    df_date_list = pd.DataFrame(date_list, columns=['date_column'])
    ascending = True if order == 'ascending' else False
    df_date_list.sort_values(by='date_column', ascending=ascending, inplace=True)
    df_date_list.reset_index(drop=True, inplace=True)
    df_date_list.index += 1
    df_date_list.reset_index(drop=False, inplace=True)
    df_date_list['index'] = df_date_list['index'].astype(int).astype(str)
    df_date_list['index'] = df_date_list['index'].str.zfill(2)
    df_date_list['date_column'] = df_date_list['date_column'].dt.strftime(date_format)
    df_date_list.rename(columns={'index': number_column, 'date_column': date_column}, inplace=True)
    df[date_column] = df[date_column].astype(str)
    df = df.merge(df_date_list, how='left', on=date_column)
    return df


def unzip_folder(command_row):
    """

    @param command_row:
    @return:
    """
    import zipfile
    folder_path = ast.literal_eval(command_row['folder path'].replace('\\', '/'))
    destination_path = ast.literal_eval(command_row['destination path'].replace('\\', '/'))

    with zipfile.ZipFile(folder_path, 'r') as zip_ref:
        zip_ref.extractall(destination_path)
    return True


def move_file(command_row):
    """

    @param command_row:
    @return:
    """
    import shutil
    source = ast.literal_eval(command_row['source'].replace('\\', '/'))
    destination = ast.literal_eval(command_row['destination'].replace('\\', '/'))
    shutil.move(source, destination)
    return True


def mdm_mapping(df, command_row):
    """

    @param df:
    @param command_row:
    @return:
    """
    # connect to db
    import pyodbc

    server = ast.literal_eval(command_row["server"])  # 'XSNW11T196B'
    database = ast.literal_eval(command_row["database"])  # 'SINERGI_DLA_VX'

    conn_str = 'DRIVER={SQL Server};;SERVER=' + server + ';DATABASE=' + database + ';UID=sauser;PWD=Solutionec@123$'
    cnxn = pyodbc.connect(conn_str)
    cursor = cnxn.cursor()
    # # get data from table
    df_mdm = pd.read_sql_query("SELECT TOP(100) * FROM dbo.PTG_dmt_ref_Transcode", cnxn)
    # df.to_csv('out.csv', index=False)

    # # map based on given column
    left_on = []
    right_on = []
    for col_names in command_row['left on']:
        df[col_names] = [str(col_value).upper().strip() for col_value in df[col_names]]
        left_on.append(col_names)
    for col_names in command_row['right on']:
        df_mdm[col_names] = [str(col_value).upper().strip() for col_value in df_mdm[col_names]]
        right_on.append(col_names)

    # if 'drop duplicates' in command_row.keys():
    #     drop_duplicates_left, drop_duplicates_right = command_row['drop duplicates'].split(',')
    #     drop_duplicates_left = drop_duplicates_left.strip().lower()[1:-1]
    #     drop_duplicates_right = drop_duplicates_right.strip().lower()[1:-1]
    #     if drop_duplicates_left == 'true':
    #         df_1.drop_duplicates(subset=left_on, inplace=True)
    #     if drop_duplicates_right == 'true':
    #         df_2.drop_duplicates(subset=right_on, inplace=True)
    df_mdm.drop_duplicates(subset=right_on, inplace=True)

    df_merged = df.merge(df_mdm, how=command_row['join type'], left_on=left_on, right_on=right_on)
    if 'select columns' in command_row.keys():
        df_merged = df_merged[ast.literal_eval(command_row['select columns'])]

    return 0


# def get_week_number(df, command_row):
#     date_col = ast.literal_eval(command_row['date column'])
#     date_format = ast.literal_eval(command_row['date format'])
#     result_col = ast.literal_eval(command_row['wk_num'])
#
#     if len(df[pd.isna(df[date_col])]) !=0:
#         print('Removed ', len(df[pd.isna(df[date_col])]), ' rows as date column was blank!')
#
#     df = df[pd.notnull(df[date_col])].copy()
#
#     # if command_row['axis'] == 'column':
#     if date_format.lower().strip() == 'excel dates':
#         dt = [datetime(*xlrd.xldate_as_tuple(int(dates), 0)) for dates in df[date_col]]
#     elif date_format.lower().strip() == 'date' or date_format.lower().strip() == 'dates':
#         # df[command_row['column_name']] = [date_5Y37.split(' ')[0] for date_5Y37 in df[command_row['column_name']]]
#         # df[command_row['column_name']] = pd.to_datetime(df[command_row['column_name']], format='%d-%m-%Y')  # remove this line
#         if not isinstance(df[date_col][0], datetime):
#             dt = pd.to_datetime(df[date_col])
#     else:
#         dt = pd.to_datetime(df[date_col], format=date_format)
#
#     # get week number


def save_to_file(df, command_row):
    # print(command_row)
    command_row['file path'] = command_row['file path'][1:-1]  # ast.literal_eval(command_row['file path'])
    # command_row['file name'] = command_row['file name'][1:-1] # ast.literal_eval(command_row['file name'])
    command_row['file path'] = command_row['file path'].replace('\\', '/')
    if command_row['file path'][-1] == '/':
        command_row['file path'] = command_row['file path'][:-1]

    if 'add_date(' in command_row['file name'].lower().strip():
        command_row['date column'] = ast.literal_eval(command_row['date column'])
        command_row['date format'] = ast.literal_eval(command_row['date format'])

        dates = list(set(df[command_row['date column']]))
        dates = [datetime.strptime(date, command_row['date format']) for date in dates if pd.notna(date)]
        dates.sort(reverse=True)
        date = dates[0]
        # date = datetime.strptime(date, command_row['date_format'])
        file_name = command_row['file name']
        file_name_list = file_name.split('"')
        for index in range(0, len(file_name_list)):
            if 'add_date(' in file_name_list[index].lower():
                element = file_name_list[index]
                element = element.split('(')[1]
                element = element.split(')')[0]
                element = datetime.strftime(date, element)
                file_name_list[index] = element

        file_name = ''.join(file_name_list)
        command_row['file name'] = file_name

    file_name = command_row['file name']
    file_name = file_name.replace('"', '')
    file_name = file_name.replace("'", '')
    command_row['file name'] = file_name

    if file_name.rsplit('.', 1)[1].lower() == 'csv':
        df.to_csv(command_row['file path'] + '/' + command_row['file name'], index=False)
    elif file_name.rsplit('.', 1)[1].lower() == 'xlsx' or file_name.rsplit('.', 1)[1].lower() == 'xlsb':
        if 'sheet name' in command_row.keys():
            sheet_name = ast.literal_eval(command_row['sheet name'])
        else:
            sheet_name = 'Sheet1'
        df.to_excel(command_row['file path'] + '/' + command_row['file name'], sheet_name=sheet_name, index=False)
    print("no. of rows: ", df.shape[0])
    return True


# def get_command_row(row):
#     command = row[0]
#     print('\t ', command)
#     command = command.lower().strip()
#     row = row.str.split('=', n=1, expand=True)
#     row[0] = row[0].str.strip().str.lower()
#     row[1] = row[1].str.strip()
#     row.iloc[0,] = ['command', command]
#     end_index = row.index[row[0] == 'finish'].tolist()[0]
#     row = row[:end_index]
#     # row[0] = row[0].str.strip().str.lower()
#     row.set_index(0, inplace=True)
#     row = pd.Series(row[1])
#     # print(row)
#     return row


# receive a df (from one sheet of excel) containing all commands
# and arguments, iterate through each command and call required function

def process_raw_file(configuration_file_name):
    try:
        df_configuration = module_configuration.get_configuration_file(configuration_file_name)
        process_each_function(df_configuration)
        return "Success"
    except Exception as e:
        return e


def process_each_function(command_df):
    inmemory_data = {}

    # loop to iterate to each command
    for _, row in command_df.iterrows():
        command_row = module_configuration.get_command_row(row)

        if command_row.index[1] == 'sub table':  # ['sob_ad', Filter=[['Dynamic_Cleaned','contains','neg.']]]
            sub_table = ast.literal_eval(command_row['sub table'])
            table_name = str(sub_table[0]).lower().strip()
            filter = sub_table[1]
            temp_command_row = pd.Series(['operation', 'temp_sub_df', str(filter)],
                                         index=['command', 'table', 'filter'])

            df_primary = inmemory_data[table_name]
            df_primary = df_primary.reset_index(drop=True)
            df_primary.index.name = 'temp_index_sub_table_df_pk'
            df_primary = df_primary.reset_index(drop=False)
            inmemory_data[table_name] = df_primary.copy()

            inmemory_data['temp_sub_df'] = df_operation(inmemory_data[table_name], temp_command_row)
            command_row = command_row.append(pd.Series(['\'temp_sub_df\''], index=['table']))

        if command_row['command'] == 'load file':
            inmemory_data[ast.literal_eval(command_row['store']).lower()] = read_file(command_row)
            if inmemory_data[ast.literal_eval(command_row['store']).lower()].empty:
                print("No data to read!")
                print(command_row)
                break
        elif command_row['command'] == 'load ftp':
            inmemory_data[ast.literal_eval(command_row['store']).lower()] = read_ftp(command_row)
            if inmemory_data[ast.literal_eval(command_row['store']).lower()].empty:
                print("No data to read!")
                print(command_row)
                break
        elif command_row['command'] == 'load ftp folder':
            status = download_ftp_folder(command_row)
            if status == 'error':
                print("No data to read!")
                print(command_row)
                break
        elif command_row['command'] == 'load fixed width file':
            inmemory_data[ast.literal_eval(command_row['store']).lower()] = read_fwf(command_row)
            if inmemory_data[ast.literal_eval(command_row['store']).lower()].empty:
                print("No data to read!")
                print(command_row)
                break
        elif command_row['command'] == 'operation':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = df_operation(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'date filter':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = date_slice(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'change date format':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = change_date_format(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'join':
            df_1_n, df_2_n = command_row['tables'].split(',')
            inmemory_data[ast.literal_eval(df_1_n).lower()] = merge_df(
                inmemory_data[ast.literal_eval(df_1_n.strip()).lower()],
                inmemory_data[ast.literal_eval(df_2_n.strip()).lower()], command_row)
        elif command_row['command'] == 'add column':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = add_column(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'add column copy':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = add_column_copy(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'fill null':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = fill_na(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'pivot':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = pivot(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'unpivot':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = unpivot(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'append':
            df_1_n, df_2_n = ast.literal_eval(command_row['tables'])  # command_row['tables'].split(',')
            inmemory_data[df_1_n.lower()] = append_table(inmemory_data[df_1_n.strip().lower()],
                                                         inmemory_data[df_2_n.strip().lower()], command_row)
        elif command_row['command'] == 'split column':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = split_column(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'replace':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = replace(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'replace character':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = replace_character(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'drop null':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = drop_null(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'combine columns':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = combine_columns(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'get max value':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = get_max_value(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'change data type':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = change_data_type(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'mdm mapping':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = mdm_mapping(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'copy table':
            inmemory_data[ast.literal_eval(command_row['store']).lower()] = inmemory_data[
                ast.literal_eval(command_row['table']).lower()].copy()
        elif command_row['command'] == 'drop duplicate':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = drop_duplicate(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'generate row id':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = generate_row_id(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'generate date number':
            inmemory_data[ast.literal_eval(command_row['table']).lower()] = generate_date_number(
                inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        elif command_row['command'] == 'unzip folder':
            status = unzip_folder(command_row)
        elif command_row['command'] == 'move file':
            status = move_file(command_row)
        elif command_row['command'] == 'save file':
            status = save_to_file(inmemory_data[ast.literal_eval(command_row['table']).lower()], command_row)
        if command_row.index[1] == 'sub table':
            # inmemory_data[table_name] -- all
            # inmemory_data['temp_sub_df'] --worked on
            # index: temp_index_sub_table_df_pk
            df_rest = pd.merge(inmemory_data[table_name], inmemory_data['temp_sub_df']['temp_index_sub_table_df_pk'],
                               on=['temp_index_sub_table_df_pk'], suffixes=('', '_y'), how='outer',
                               indicator=True).query(
                "_merge == 'left_only'").drop('_merge', axis=1).reset_index(drop=True)
            df = df_rest.append(inmemory_data['temp_sub_df'])
            df = df.reset_index(drop=True)
            df.drop(columns=['temp_index_sub_table_df_pk'], inplace=True)
            inmemory_data[table_name] = df.copy()
            df = df_rest = df_primary = inmemory_data['temp_sub_df'] = temp_command_row = ''


# def logger_configuration():
#     # Create a custom logger
#     # logger = logging.getLogger(__name__)
#     # logger.setLevel(logging.DEBUG)
#
#     log_file_name_suffix = datetime.now().strftime('%d%b%Y_%H_%M_%S')
#
#     f_handler = logging.FileHandler("log/LOG"+log_file_name_suffix+".log", mode='w')
#     f_handler.setLevel(logging.INFO)
#
#     f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt='%d-%b-%Y %H:%M:%S')
#     f_handler.setFormatter(f_format)
#
#     logger.addHandler(f_handler)
#
#     # logger.debug('This is a debug message')
#     # logger.info('This is an info message')
#     # logger.warning('This is a warning message')
#     # print('This is an error message')
#     # logger.critical('This is a critical message')


def main(configuration_file_name):
    process_raw_file(configuration_file_name)
    # logger_configuration()
    # logger = logging.getLogger(__name__)

    # directory_temp = r"C:\Users\Dell\Desktop\office_data\file_based_automation\sit_automation\config_files"

    # files = os.listdir(directory_temp)
    # file_list = [file for file in files if re.search('automation_5y37', file.lower()) and '~$' not in file]
    # if len(file_list) == 0:
    #     print('Skipping as no configuration file available!')
    #     # print()
    #     return None
    # if len(file_list) > 1:
    #     print('Multiple configuration files found:  ', file_list)
    #     return None
    #
    # metadata_path = file_list[0]
    #
    # print('Metadata path is:  ' + directory_temp + '/' + metadata_path)
    # metadata_df_dict = pd.read_excel(directory_temp + '/' + metadata_path, sheet_name=None, header=None, skiprows=1)
    # for key in metadata_df_dict.keys():
    #     if key.lower().strip() == 'run':
    #         metadata_df_dict.pop(key)
    #         break
    #
    # if isinstance(metadata_df_dict, pd.DataFrame):
    #     if metadata_df_dict.iloc[0, 1].lower().strip() in ['true', 'yes', 'y']:
    #         process_file(metadata_df_dict)
    #     else:
    #         # logger.info('Sheet Name:', key)
    #         print("Skipping as requested!")
    # elif isinstance(metadata_df_dict, dict):
    #     for key, metadata_df in metadata_df_dict.items():
    #         print('\n')
    #         print('Sheet Name: ', key)
    #         if str(metadata_df.iloc[0, 1]).lower().strip() in ['true', 'yes', 'y']:
    #             # print('\tCommands:')
    #             try:
    #                 process_file(metadata_df)
    #                 print("Complete!")
    #             except Exception as e:
    #                 # logger.exception('Sheet name: ', key)
    #                 print()
    #                 print('*' * 40, 'AN ERROR OCCURRED', '*' * 40)
    #                 print('ERROR MESSAGE:')
    #                 print('\t', e)
    #                 print()
    #                 print('-' * 90)
    #                 print('ERROR TRACEBACK STACK:\n')
    #                 # print(e)
    #                 traceback.print_exc(file=sys.stdout)
    #                 # print('*'*100)
    #                 # print(metadata_df)
    #         else:
    #             print("-- Skipping as requested!")


if __name__ == '__main__':
    main('default_configuration_file')
