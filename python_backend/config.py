import pandas as pd


def get_configuration_file(configuration_file_name: str):
    config_file = pd.read_excel(configuration_file_name)
    return config_file


def get_command_row(row):
    command = row[0]
    print('\t ', command)
    command = command.lower().strip()
    row = row.str.split('=', n=1, expand=True)
    row[0] = row[0].str.strip().str.lower()
    row[1] = row[1].str.strip()
    row.iloc[0,] = ['command', command]
    end_index = row.index[row[0] == 'finish'].tolist()[0]
    row = row[:end_index]
    # row[0] = row[0].str.strip().str.lower()
    row.set_index(0, inplace=True)
    row = pd.Series(row[1])
    # print(row)
    return row
